import React from "react";
import BookService from "../services/BookService";


class AddBook extends React.Component{
    constructor(props) {
        super(props);
        this.onChangeTitle = this.onChangeTitle.bind(this);
        this.onChangeAuthor = this.onChangeTitle.bind(this);
        this.onChangeGenre = this.onChangeGenre.bind(this);
        this.onChangeLocation = this.onChangeLocation.bind(this);
        this.onChangeOwner = this.onChangeOwner.bind(this);
        this.saveBook = this.saveBook.bind(this);
        this.newBook = this.newBook.bind(this);
        this.state = {
            id: null,
            title: "",
            author: "",
            genre: "",
            location: "",
            owner: "",
            published: false,
            submitted: false
        };
    }
    onChangeTitle(e) {
        this.setState({
            title: e.target.value
        });
        console.log("Title:", this.state.title)
    }

    onChangeAuthor(e) {
        this.setState({
            author: e.target.value
        });
        console.log("Author:", this.state.author)
    }

    onChangeGenre(e) {
        this.setState({
            genre: e.target.value
        });
        console.log("Genre:", this.state.genre)
    }
    onChangeLocation(e) {
        this.setState({
            location: e.target.value
        });
        console.log("Location:", this.state.location)
    }

    onChangeOwner(e) {
        this.setState({
            owner: e.target.value
        });
        console.log("Owner:", this.state.owner)
    }

    saveBook() {
        let data = {
            title: this.state.title,
            author: this.state.author,
            genre: this.state.genre,
            location: this.state.location,
            owner: this.state.owner
        }
        BookService.create(data)
            .then(response => {
                this.setState({
                    id: response.data.id,
                    title: response.data.title,
                    author: response.data.author,
                    genre: response.data.genre,
                    location: response.data.location,
                    owner: response.data.ownder,
                    published: response.data.published,
                    submitted: true
                })
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            })
    }
    newBook() {
        this.setState({
            id: null,
            title: "",
            author: "",
            genre: "",
            location: "",
            owner: "",
            published: false,
            submitted: false
        })
    }

    render() {
        return (
            <div className="submit-form">
                {this.state.submitted ? (
                    <div>
                        <h4>You submitted successfully!</h4>
                        <button className="btn btn-success" onClick={this.newBook}>
                            Add
                        </button>
                    </div>
                ) : (
                    <div>
                        <div className="form-group">
                            <label htmlFor="title">Title</label>
                            <input
                                type="text"
                                className="form-control"
                                id="title"
                                requiredvalue={this.state.title}
                                onChange={this.onChangeTitle}
                                name= "title"
                                />
                        </div>
                    <div className="form-group">
                    <label htmlFor="author">Author</label>
                    <input
                        type="text"
                        className="form-control"
                        id="author"
                        requiredvalue={this.state.author}
                        onChange={this.onChangeAuthor}
                        name= "author"
                        />
                </div>
                <div className="form-group">
                <label htmlFor="genre">Genre</label>
                <input
                    type="text"
                    className="form-control"
                    id="genre"
                    requiredvalue={this.state.genre}
                    onChange={this.onChangeGenre}
                    name= "genre"
                    />
            </div>
            <div className="form-group">
            <label htmlFor="location">Location</label>
            <input
                type="text"
                className="form-control"
                id="location"
                requiredvalue={this.state.location}
                onChange={this.onChangeLocation}
                name= "location"
                />
            </div>
            <div className="form-group">
            <label htmlFor="owner">Owner</label>
            <input
                type="text"
                className="form-control"
                id="owner"
                requiredvalue={this.state.owner}
                onChange={this.onChangeOwner}
                name= "owner"
                />
            </div>
            <button onClick={this.saveBook} className="btn btn-success">
                Submit
            </button>
            </div>
                )}
            </div>
        );
    }

}

export default AddBook;
