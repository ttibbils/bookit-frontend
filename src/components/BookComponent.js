import React from 'react';
import BookService from '../services/BookService';

class BookComponent extends React.Component {

    constructor(props){
        super(props)
        this.state = {
            books:[]
        }
    }

    componentDidMount(){
        BookService.getAll().then((response) => {
            this.setState({ books: response.data})
        })
    }

    deleteButton(id){
        return (
            <button onClick={() => BookService.delete(id)}>Delete</button>
        )
    }

    render(){
        return (
            <div>
                <h1 className = "text-center">Available Books</h1>
                <table className = "table table-striped">
                    <thead>
                        <tr>
                            <th>Book ID</th>
                            <th>Title</th>
                            <th>Author</th>
                            <th>Genre</th>
                            <th>Location</th>
                            <th>Owner</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.books.map(
                                book =>
                                <tr key = {book.id}>
                                    <td> {book.id} </td>
                                    <td> {book.title} </td>
                                    <td> {book.author} </td>
                                    <td> {book.genre} </td>
                                    <td> {book.location} </td>
                                    <td> {book.owner} </td>
                                    <td> {this.deleteButton(book.id)} </td>
                                </tr>
                            )
                        }

                    </tbody>
                </table>
            </div>
        )
    }
}

export default BookComponent