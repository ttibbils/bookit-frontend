import './App.css';
import BookComponent from './components/BookComponent';
import AddBook from './components/AddBookComponent';




function App() {
  return (
    <div className="App">
        <BookComponent />
        <AddBook />
    </div>
  );
}


export default App;
